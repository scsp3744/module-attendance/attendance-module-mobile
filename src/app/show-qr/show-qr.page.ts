import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ModalController } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
// import QRCode from 'qrcode';

@Component({
  selector: 'app-show-qr',
  templateUrl: './show-qr.page.html',
  styleUrls: ['./show-qr.page.scss'],
})
export class ShowQRPage implements OnInit {
  code: any;
  generated = '';
  @Input() recent: any;
  encodeData: any;
  constructor(public modalCtrl: ModalController, public http: HttpClient, private barcodeScanner: BarcodeScanner) {
  }

  ngOnInit() {
    console.log(this.recent)
    this.code = this.recent.key;
    this.encodeData = "http://localhost:8080/api/attendance/scan/" + this.code;
    // const qrcode = QRCode;
    // qrcode.toDataURL(this.code, { errorCorrectionLevel: 'H' }, function (err, url) {
    //   this.generated = url;
    // })
  }
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
  encodedText() {
    this.barcodeScanner
      .encode(this.barcodeScanner.Encode.TEXT_TYPE, this.encodeData)
      .then(
        encodedData => {
          console.log(encodedData);
          this.encodeData = encodedData;
        },
        err => {
          console.log("Error occured : " + err);
        }
      );
  }
}
