import { Component, OnInit } from '@angular/core';
import { AddRequestPage } from '../add-request/add-request.page';
import { ModalController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-student',
  templateUrl: './student.page.html',
  styleUrls: ['./student.page.scss'],
})
export class StudentPage implements OnInit {
  requests: any = [];
  sid: any;
  name: any;
  constructor(public modalCtrl: ModalController, public http: HttpClient) {
    // will be replaced once combined with login
    localStorage.setItem('sid', '5eb1d3fb4b8ffee90e1c6983');
    localStorage.setItem('name', 'Johnathan');

    // localStorage.setItem('sid', '5eb1d3fbb68aa629ca8f1aab');
    // localStorage.setItem('name', 'April');

    this.sid = localStorage.getItem('sid');
    this.name = localStorage.getItem('name')
  }
  // new ionic lifecycle method. will be called after the page is loaded
  // more info here https://ionicframework.com/docs/angular/lifecycle
  ionViewDidEnter() {
    // this.reviewService.getReviews().then((data) => {
    //   console.log(data);
    //   this.reviews = data;
    // });
    this.http.get('http://localhost:8080/api/requests/' + this.sid).subscribe(data => {
      this.requests = data
      console.log(data)
    })
    // this.requests = [{ Title: "abc" }, { Title: "haha" }]
  }

  async showModal() {
    const modal = await this.modalCtrl.create({
      component: AddRequestPage
    });
    await modal.present();

    // process the data from modal as the modal's call dismiss func 
    // returned is a JSON object, eg { data: { title: 'abc', rating: 50 } }
    // therefore, use { data } (object deconstructing) to extract it
    const { data } = await modal.onDidDismiss();

    if (data) {
      var send = {
        sid: localStorage.getItem('sid'),
        cid: data.SelectedCourseID,
        reason: data.reason,
        requestOn: data.date
      }
      this.http.post('http://localhost:8080/api/requestLeave', send).subscribe(data => {
        if (data['message'] != 'Leave requested.')
          alert(data['message']);
        this.ionViewDidEnter();
      })
      console.log(data)
    }
  }

  delete(request) {
    // console.log(requestId)
    //Remove locally

    // console.log(review);
    // //Remove from database
    var send = {
      sid: this.sid,
      requestID: request._id
    }
    this.http.post('http://localhost:8080/api/deleteRequest', send).subscribe(data => {
      if (data['message'] == 'Ok') {
        let index = this.requests.indexOf(request);

        if (index > -1) {
          this.requests.splice(index, 1);
        }
      }

    })
  }
  ngOnInit() {
  }

}
