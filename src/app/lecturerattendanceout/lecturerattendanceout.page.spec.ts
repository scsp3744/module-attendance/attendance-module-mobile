import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LecturerattendanceoutPage } from './lecturerattendanceout.page';

describe('LecturerattendanceoutPage', () => {
  let component: LecturerattendanceoutPage;
  let fixture: ComponentFixture<LecturerattendanceoutPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LecturerattendanceoutPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LecturerattendanceoutPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
