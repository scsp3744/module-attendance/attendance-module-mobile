import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LecturerattendancePageRoutingModule } from './lecturerattendance-routing.module';

import { LecturerattendancePage } from './lecturerattendance.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LecturerattendancePageRoutingModule
  ],
  declarations: [LecturerattendancePage]
})
export class LecturerattendancePageModule {}
