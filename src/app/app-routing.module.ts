import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'show-qr',
    loadChildren: () => import('./show-qr/show-qr.module').then( m => m.ShowQRPageModule)
  },
  {
    path: 'register-attendance',
    loadChildren: () => import('./register-attendance/register-attendance.module').then( m => m.RegisterAttendancePageModule)
  },
  {
    path: 'student',
    loadChildren: () => import('./student/student.module').then( m => m.StudentPageModule)
  },
  {
    path: 'add-request',
    loadChildren: () => import('./add-request/add-request.module').then( m => m.AddRequestPageModule)
  },
  {
    path: 'start-attendance',
    loadChildren: () => import('./start-attendance/start-attendance.module').then( m => m.StartAttendancePageModule)
  },
  {
    path: 'checkattendance',
    loadChildren: () => import('./checkattendance/checkattendance.module').then( m => m.CheckattendancePageModule)
  },
  {
    path: 'lecturerattendance',
    loadChildren: () => import('./lecturerattendance/lecturerattendance.module').then( m => m.LecturerattendancePageModule)
  },
  {
    path: 'lecturerattendancein',
    loadChildren: () => import('./lecturerattendancein/lecturerattendancein.module').then( m => m.LecturerattendanceinPageModule)
  },
  {
    path: 'lecturerattendanceout',
    loadChildren: () => import('./lecturerattendanceout/lecturerattendanceout.module').then( m => m.LecturerattendanceoutPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
