import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LecturerattendanceinPage } from './lecturerattendancein.page';

const routes: Routes = [
  {
    path: '',
    component: LecturerattendanceinPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LecturerattendanceinPageRoutingModule {}
